from behave import *


@given(u'the user has the correct credentials')
def step_impl(context):
    context.browser.get('https://sprinkle-burn.glitch.me/')
    assert context.browser.title == 'Worlds Best App'


@when(u'the user enters username')
def step_impl(context):
    context.browser.find_element_by_name('email').send_keys('test@drugdev.com')


@step(u'the user enters password')
def step_impl(context):
    context.browser.find_element_by_name('password').send_keys('supers3cret')


@step(u'clicks Login')
def step_impl(context):
    context.browser.find_element_by_tag_name('button').click()


@then(u'the user is presented with a welcome message')
def step_impl(context):
    assert context.browser.page_source.find("Welcome Dr I Test")


@given(u'a set of invalid credentials')
def step_impl(context):
    context.browser.get('https://sprinkle-burn.glitch.me/')
    assert context.browser.title == 'Worlds Best App'


@when('the user enters invalid "(?P<username>.+)"')
def step_impl(context, username):
    if username == "empty":
        usr = ""
    else:
        usr = username
    context.browser.find_element_by_name('email').send_keys(usr)


@step(u'the user enters invalid "(?P<password>.+)"')
def step_impl(context, password):
    if password == "empty":
        pwd = ""
    else:
        pwd = password
    context.browser.find_element_by_name('password').send_keys(pwd)


@then(u'the user is presented with a error message')
def step_impl(context):
    assert context.browser.page_source.find('Credentials are incorrect')
