Feature: Login
  In order to use the app the user must be able to Login

  Scenario: Login Success
    Given the user has the correct credentials
    When the user enters username
    And the user enters password
    And clicks Login
    Then the user is presented with a welcome message

  Scenario Outline: Login Incorrect credentials
    Given a set of invalid credentials
    When the user enters invalid "<username>"
    And the user enters invalid "<password>"
    And clicks Login
    Then the user is presented with a error message

    Examples:
      |username           |password   |
      |test@drugdev.com   |pwd123     |
      |inv@drugdev.com    |supers3cret|
      |inv@drugdev.com    |pwd123     |
      |test@drugdev.com   |empty      |
      |empty              |supers3cret|
      |empty              |empty      |